import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import Button from '../components/Button'
import UserForm from '../components/UserForm'
import { useMutation, useApolloClient, gql } from '@apollo/client'

const SIGNUP_USER = gql`
    mutation signUp($email: String!, $username: String!, $password: String!){
        signUp(email: $email, username: $username, password: $password)
    }
`

const SignUp = props => {
    const [values, setValues] = useState()

    useEffect(() => {
        document.title = 'Sign Up - Notedly'
    })

    const onChange = event => {
        setValues({
            ...values,
            [event.target.name]: event.target.value
        })
    }

    const client = useApolloClient()

    const [signUp, {loading, error}] = useMutation(SIGNUP_USER, {
        onCompleted: data => {
            console.log(data.signUp)
            localStorage.setItem('token', data.signUp)
            client.writeData({data: {isLoggedIn: true}})
            props.history.push('/')
        }
    })

    return (
        <React.Fragment>
            <UserForm action={signUp} formType="signup" />
            {loading && <p>Loading data ...</p>}
            {error && <p>Error creating an account</p>}
        </React.Fragment>
    )
}

export default SignUp

const Wrapper = styled.div`
    border: 1px solid #f5f4f0;
    max-width: 500px;
    padding: 1em;
    margin: 0 auto;
`;

const Form = styled.form`
  label,
  input {
    display: block;
    line-height: 2em;
  }

  input {
    width: 100%;
    margin-bottom: 1em;
  }
`;